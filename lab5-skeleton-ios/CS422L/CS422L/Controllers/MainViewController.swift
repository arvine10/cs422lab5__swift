//
//  ViewController.swift
//  CS422L
//
//  Created by Jonathan Sligh on 1/29/21.
//

import Foundation
import UIKit
import CoreData

class MainViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource  {
    
    var flashcardsetentites: [FlashCardSetEntity] = []
 
    @IBOutlet var collectionView: UICollectionView!
     
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        sets = FlashcardSet.getHardCodedCollection()
        collectionView.delegate = self
        collectionView.dataSource = self
        makeThingsLookPretty()
        
        let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "FlashCardSetEntity")
        
        do {
            let flashs = try context.fetch(fetchRequest) as! [FlashCardSetEntity]
            updateData(flashCards: flashs)
        } catch {
            print(error)
        }
        
    }

    
    @IBAction func addNewSet(_ sender: Any) {
//        let newSet = FlashcardSet()
//        newSet.title = "Title \(sets.count + 1)"
//        sets.append(newSet)
        
        let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
        let newCard = NSEntityDescription.insertNewObject(forEntityName:  "FlashCardSetEntity", into: context) as! FlashCardSetEntity
        newCard.term = "newTerm"
        do {
            try context.save()
        }
        catch{
            print(error)
        }
        flashcardsetentites.append(newCard)
        collectionView.reloadData()
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return flashcardsetentites.count
    }
    
    // ask about FlashCardSetCollectionCell
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SetCell", for: indexPath) as! FlashcardSetCollectionCell
        //setup method just makes it look nice
        cell.setup()
        cell.textLabel.text = flashcardsetentites[indexPath.row].term
        return cell
    }
    

    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        //go to new view
        performSegue(withIdentifier: "GoToDetail", sender: self)
    }
    
    //another function to make things look nice
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {

        let noOfCellsInRow = 2   //number of column you want
        let flowLayout = collectionViewLayout as! UICollectionViewFlowLayout
        let totalSpace = flowLayout.sectionInset.left
            + flowLayout.sectionInset.right
            + (flowLayout.minimumInteritemSpacing * CGFloat(noOfCellsInRow - 1))

        let size = Int((collectionView.bounds.width - totalSpace) / CGFloat(noOfCellsInRow))
        return CGSize(width: size, height: size)
    }
    
    //just a function to make things look nice
    func makeThingsLookPretty()
    {
        let margin: CGFloat = 10
        guard let collectionView = collectionView, let flowLayout = collectionView.collectionViewLayout as? UICollectionViewFlowLayout else { return }
        flowLayout.minimumInteritemSpacing = margin
        flowLayout.minimumLineSpacing = margin
        flowLayout.sectionInset = UIEdgeInsets(top: margin, left: margin, bottom: margin, right: margin)
    }
    
    
    func updateData(flashCards: [FlashCardSetEntity]) {
        self.flashcardsetentites = flashCards
        self.collectionView.reloadData()
    }
    
    
}

