//
//  EditAlertViewController.swift
//  CS422L
//
//  Created by Jonathan Sligh on 2/18/21.
//

import UIKit

class EditAlertViewController: UIViewController {
    var index : Int = Int()
    @IBOutlet var alertView: UIView!
    @IBOutlet var termEditText: UITextField!
    @IBOutlet var definitionEditText: UITextField!
    //card from FlashCardSetDetailViewController
    var card: FlashCardEntity = FlashCardEntity()
    //use this later to do things to the flashcards potentially
    var parentVC: FlashCardSetDetailViewController?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        setup()
    }
    
    func setup()
    {
        alertView.layer.cornerRadius = 8.0
        //set term/def
        termEditText.text = card.term
        definitionEditText.text = card.defintion
        //make it so it shows this is editable
        termEditText.becomeFirstResponder()
    }
    
    @IBAction func deleteFlashcard(_ sender: Any) {
        //nothing yet but eventually delete the flashcard
        self.dismiss(animated: false, completion: nil)
        
        let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
        do {
            context.delete(card)
            try context.save()
            
            // how do i remove from list and redraw the table
//            self.FlasCardEntity.remove(at: indexPath.row)
//            tableView.reloadData()
        } catch {}
        
        self.parentVC?.flashcardentites.remove(at: index)
        self.parentVC?.tableView.reloadData()
        
        
    }
    
    @IBAction func doneEditing(_ sender: Any) {
        self.dismiss(animated: false, completion: {
            //do something / save the edits
            let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
            self.card.term = self.termEditText.text
            self.card.defintion = self.definitionEditText.text
            do {
                try context.save()
                
            }
            catch{
                print(error)
            }
            self.parentVC?.tableView.reloadData()
            
        })
    }
    
}
